import { MatIconModule, MatToolbarModule, MatDialogModule, MatButtonModule, MatCardModule, MatTabsModule, MatProgressSpinnerModule, MatMenuModule, MatInputModule, MatCheckboxModule, MatProgressBarModule, MatSidenavModule, MatPaginatorModule, MatAutocompleteModule, MatSnackBarModule, MatTooltipModule, MatSelectModule, MatListModule, MatSlideToggleModule, MatExpansionModule, MatChipsModule } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SidenavComponent } from './sidenav/sidenav.component';
import { LoadingService } from './services/loading.service';
import { GeneralService } from './services/api/general.service';
import { ApplicationService } from './services/application.service';
import { FreePlacesSignComponent } from './components/shared/free-places-sign/free-places-sign.component';
import { EtageComponent } from './components/shared/etage/etage.component';
import { ParkhausOverviewComponent } from './components/shared/parkhaus-overview/parkhaus-overview.component';
import { EtageHeaderComponent } from './components/shared/etage-header/etage-header.component';
import { EtageInfoComponent } from './components/shared/etage-info/etage-info.component';
import { ParkhausPageComponent } from './components/parkhaus-page/parkhaus-page.component';
import { ParkhausFreeLotsComponent } from './components/shared/parkhaus-free-lots/parkhaus-free-lots.component';
import { CarComponent } from './components/shared/car/car.component';
import { ParkplatzComponent } from './components/shared/parkplatz/parkplatz.component';
import { CarDetailsDialogComponent } from './dialogs/car-details-dialog/car-details-dialog.component';
import { ResetParkhausPageComponent } from './components/reset-parkhaus-page/reset-parkhaus-page.component';
import { ParkhausStatistikPageComponent } from './components/parkhaus-statistik-page/parkhaus-statistik-page.component';

const routes: Routes = [
  { path: 'main-page', component: MainPageComponent },
  { path: 'parkhaus-page', component: ParkhausPageComponent },
  { path: 'reset-parkhaus-page', component: ResetParkhausPageComponent },
  { path: 'parkhaus-statistik-page', component: ParkhausStatistikPageComponent },
  {
    path: '',
    redirectTo: '/main-page',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CarComponent,
    MainPageComponent,
    PageNotFoundComponent,
    SidenavComponent,
    FreePlacesSignComponent,
    EtageComponent,
    ParkhausOverviewComponent,
    EtageHeaderComponent,
    EtageInfoComponent,
    ParkhausPageComponent,
    ParkhausFreeLotsComponent,
    ParkplatzComponent,
    CarDetailsDialogComponent,
    ResetParkhausPageComponent,
    ParkhausStatistikPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatCardModule,
    MatChipsModule,
    MatTabsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatSelectModule,
    MatListModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    MatMenuModule,
    RouterModule.forRoot(
      routes,
      { useHash: true }
    )
  ],
  providers: [LoadingService, GeneralService, ApplicationService],
  bootstrap: [AppComponent],
  entryComponents: [CarDetailsDialogComponent]
})
export class AppModule { }
