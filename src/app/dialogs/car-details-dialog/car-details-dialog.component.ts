import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IfCarDetailsDialogData } from '../../data/dialogs/if-car-details-dialog-data';
import { GeneralService } from '../../services/api/general.service';
import { IfParkingLotInformation } from '../../data/if-parking-lot-information';
import { IfParkplatzInformation } from '../../data/if-parkplatz-information';

@Component({
  selector: 'app-car-details-dialog',
  templateUrl: './car-details-dialog.component.html',
  styleUrls: ['./car-details-dialog.component.scss']
})
export class CarDetailsDialogComponent implements OnInit {
  value: IfParkplatzInformation = null;
  constructor(private dialogRef: MatDialogRef<CarDetailsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: IfCarDetailsDialogData, private general: GeneralService) { }

  ngOnInit() {
    this.general.getParkplatzInformation(this.data.etageid, this.data.parkplatzid).then(res => {
      this.value = res;
    }).catch((err) => {
      this.onClose("error");
    });
  }

  onClose(name: string): void {
    this.dialogRef.close({ name: name });
  }

  getCurrentParkingPrice(): number {
    if (this.value) {
      let dauer = this.getParkingTime();
      let preis = this.value.preis;
      let price: number = dauer.h * preis + (dauer.m > 0 ? (preis) : 0);
      return price;
    }
    return 0;
  }

  onLeaveCar() {
    this.general.leaveCarFromParkingLot(this.data.etageid, this.data.parkplatzid).then(res => {
      if (res.foundkfz) {
        this.onClose("leave");
      } else {
        this.onClose("error");
      }
    }).catch((err) => {
      this.onClose("error");
    });
  }

  getParkingTime() {
    if (this.value && this.value.parkplatz.fahrzeug) {
      let current: number = new Date().getTime();
      let since = current - this.value.parkplatz.fahrzeug.ankunft;
      var diffMs = (since); // milliseconds between now & Christmas
      var diffDays = Math.floor(diffMs / 86400000); // days
      var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
      var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
      return { h: diffHrs, m: diffMins, d: diffDays };
    }
    return { h: 0, m: 0, d: 0 };
  }

  getAmountOfParkingHours(): string {
    return "" + this.getParkingTime().h;
  }
  getAmountOfParkingMinutes(): string {
    return "" + this.getParkingTime().m;
  }
}
