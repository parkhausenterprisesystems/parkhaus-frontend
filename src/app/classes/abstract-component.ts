import { LoadingService } from "../services/loading.service";

export class AbstractComponent {
    constructor(private loading: LoadingService) {

    }

    showLoading() {
        this.loading.showLoading();
    }

    hideLoading() {
        this.loading.hideLoading();
    }
}
