import { IfFahrzeugStatistik } from "./if-fahrzeug-statistik";

export interface IfStatistikResult {
    anzahlfahrzeuge: number;
    einnahmen: number;
    ausgaben: number;
    gewinn: number;
    jahresstatistik: {
        anzahlfahrzeuge: number,
        einnhamen: number,
        ausgaben: number,
        gewinn: number,
        monat: number,
        jahr: number,
        current: boolean
    }[];
    fahrzeuge: IfFahrzeugStatistik[];
}
