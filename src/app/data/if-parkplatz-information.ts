import { IfCar } from "./if-car";

export interface IfParkplatzInformation {
    etageid: number;
    parkplatzid: number;
    preis: number;
    parkplatz: {
        typ: string;
        belegt: boolean;
        fahrzeug?: IfCar;
    }
}
