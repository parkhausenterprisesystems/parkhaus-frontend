import { IfParkplatz } from "./if-parkplatz";

export interface IfEtage {
    id: number;
    normal: {
        frei: number;
        belegt: number;
    };
    behinderte: {
        frei: number;
        belegt: number;
    };
    frauen: {
        frei: number;
        belegt: number;
    };
    parkplaetze: IfParkplatz[];
}
