export interface IfParkingLotInformation {
    free: number;
    used: number;
}
