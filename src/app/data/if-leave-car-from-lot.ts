import { IfCar } from "./if-car";

export interface IfLeaveCarFromLot {
    foundkfz: boolean;
    fahrezug?: IfCar;
}
