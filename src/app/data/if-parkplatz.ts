import { IfCar } from "./if-car";

export interface IfParkplatz {
    id: number;
    typ: string;
    belegt: boolean;
    fahrzeug?: IfCar;
}
