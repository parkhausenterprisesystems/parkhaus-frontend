export interface IfFahrzeugStatistik {
    etage: number;
    parkplatz: number;
    einfahrt: number;
    ausfahrt: number;
    geparkt: {
        stunden: number;
        minuten: number;
        preis: number;
    };
    kennzeichen: string;
    farbe: string;
    geschlecht: string;
    behinderung: boolean;
}
