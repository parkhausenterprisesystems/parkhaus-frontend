export interface IfCar {
    wartet: boolean;
    kennzeichen: string;
    color: string;
    geschlecht: string;
    behinderung: boolean;
    ankunft?: number;
    ausfahrt?: number;
}
