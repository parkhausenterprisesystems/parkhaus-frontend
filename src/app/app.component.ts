import { Component } from '@angular/core';
import { LoadingService } from './services/loading.service';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Parkhaus Enterprise Systems';
  loader: boolean = false;
  slow: boolean = false;
  slowHandler = null;
  constructor(private loading: LoadingService, private router: Router) {

  }

  ngOnInit() {
    this.loading.addListener((show: boolean) => {
      if (show) {
        this.activateLoader();
      } else {
        this.disableLoader();
      }
    });


    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.activateLoader();
      }
      if (event instanceof NavigationEnd) {
        this.disableLoader();
      }
    });


  }

  private activateLoader() {
    this.loader = true;
    this.slow = false;
    this.slowHandler = window.setTimeout(() => {
      if (this.loader) {
        this.slow = true;
        this.slowHandler = null;
      }
    }, 11000);
  }

  private disableLoader() {
    this.loader = false;
    if (this.slowHandler != null) {
      window.clearTimeout(this.slowHandler);
      this.slowHandler = null;
    }
  }
}
