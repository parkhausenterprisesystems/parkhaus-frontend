import { Injectable } from '@angular/core';

@Injectable()
export class ApplicationService {
  private API_URL: string = "https://ultimapp.de/tomcat/parkhaus-backend/api/";
  constructor() { }

  getApiUrl(): string {
    return this.API_URL;
  }
}
