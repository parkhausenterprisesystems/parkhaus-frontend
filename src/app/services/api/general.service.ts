import { Injectable } from '@angular/core';
import { IfCar } from '../../data/if-car';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApplicationService } from '../application.service';
import { IfParkingLotInformation } from '../../data/if-parking-lot-information';
import { IfParkhausDetails } from '../../data/if-parkhaus-details';
import { IfParkplatzInformation } from '../../data/if-parkplatz-information';
import { IfLeaveCarFromLot } from '../../data/if-leave-car-from-lot';
import { IfStatistikResult } from '../../data/if-statistik-result';

@Injectable()
export class GeneralService {
  private BASE_URL = "general/";
  constructor(private http: HttpClient, private app: ApplicationService) {
  }

  createCar(): Promise<IfCar> {
    return this.sendRequest("create-car", {});
  }

  getParkingLotInformation(): Promise<IfParkingLotInformation> {
    return this.sendRequest("free-parking-lots", {});
  }

  getParkhausDetails(): Promise<IfParkhausDetails> {
    return this.sendRequest("parkhaus-details", {});
  }

  getParkplatzInformation(etageid: number, parkplatzid: number): Promise<IfParkplatzInformation> {
    return this.sendRequest("get-parkplatz", { etage: etageid, parkplatz: parkplatzid });
  }

  leaveCarFromParkingLot(etageid: number, parkplatzid: number): Promise<IfLeaveCarFromLot> {
    return this.sendRequest("leave-kfz-from-parking-lot", { etage: etageid, parkplatz: parkplatzid });
  }

  leaveRandomCar(): Promise<{ foundkfz: boolean, fahrzeug?: IfCar }> {
    return this.sendRequest("leave-random-kfz", {});
  }

  resetParkhaus(v: { etagen: number, parkinglots: number, disabledparking: number, womenparking: number, price: number }): Promise<any> {
    return this.sendRequest("reset-parkhaus", v);
  }

  getStatistik(zeitpunkt?: { monat: number, jahr: number }): Promise<IfStatistikResult> {
    return this.sendRequest("get-statistik", { zeitpunkt: zeitpunkt });
  }

  private sendRequest(path: string, body: {}): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(this.app.getApiUrl() + this.BASE_URL + path, body, { headers: new HttpHeaders().append("content-type", "application/json") }).subscribe(res => {
        resolve(res);
      }, error => {
        reject(error);
      });
    });
  }
}
