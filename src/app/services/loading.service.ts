import { Injectable } from '@angular/core';

@Injectable()
export class LoadingService {
  private listener: Function[] = [];
  private status: boolean = false;
  constructor() { }


  addListener(listener: (show: boolean) => void): void {
    this.listener.push(listener);
  }

  showLoading() {
    this.status = true;
    this.handleEvent(this.status);
  }

  hideLoading() {
    this.status = false;
    this.handleEvent(this.status);
  }

  isLoading(): boolean {
    return this.status;
  }

  private handleEvent(show: boolean) {
    this.listener.forEach(element => {
      element(show);
    });
  }
}
