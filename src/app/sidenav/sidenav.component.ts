import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  @Input("side") sidenav: any;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  close() {
    this.sidenav.close();
  }

  navigate(path: string) {
    this.close();
    this.router.navigate([path]);
  }

}
