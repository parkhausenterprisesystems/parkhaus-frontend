import { Component, OnInit } from '@angular/core';
import { IfParkhausDetails } from '../../data/if-parkhaus-details';
import { AbstractComponent } from '../../classes/abstract-component';
import { LoadingService } from '../../services/loading.service';
import { GeneralService } from '../../services/api/general.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-parkhaus-page',
  templateUrl: './parkhaus-page.component.html',
  styleUrls: ['./parkhaus-page.component.scss']
})
export class ParkhausPageComponent extends AbstractComponent implements OnInit {
  details: IfParkhausDetails = null;
  reload: Subject<any> = new Subject<any>();
  constructor(loading: LoadingService, private general: GeneralService) {
    super(loading);
  }

  ngOnInit() {
    this.onLoadData();
  }

  onLoadData() {
    this.showLoading();
    this.general.getParkhausDetails().then(res => {
      this.details = res;
      this.hideLoading();
    }).catch((err) => {
      this.hideLoading();
    });
  }

  onUpdate(event: any) {
    this.reload.next();
    this.onLoadData();
  }
}
