import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkhausPageComponent } from './parkhaus-page.component';

describe('ParkhausPageComponent', () => {
  let component: ParkhausPageComponent;
  let fixture: ComponentFixture<ParkhausPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkhausPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkhausPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
