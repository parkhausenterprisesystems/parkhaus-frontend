import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../services/api/general.service';
import { AbstractComponent } from '../../classes/abstract-component';
import { LoadingService } from '../../services/loading.service';
import { IfParkhausDetails } from '../../data/if-parkhaus-details';
import { IfCar } from '../../data/if-car';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent extends AbstractComponent implements OnInit {
  carQuery: IfCar[] = [];
  reload: Subject<any> = new Subject<any>();
  constructor(private general: GeneralService, loading: LoadingService) {
    super(loading);
  }

  ngOnInit() {

  }

  loadDetails() {
  }

  update() {
    this.reload.next();
  }

  onCreateCar() {
    this.general.createCar().then((res) => {
      this.carQuery.push(res);
      this.update();
      this.checkQuery(res);
    }).catch(err => { });
  }

  checkQuery(res: IfCar) {
    window.setTimeout(() => {
      if (!res.wartet) {
        this.carQuery.splice(this.carQuery.indexOf(res), 1);
      }
    }, 3000);
  }

  onLeaveRandomCar() {
    this.general.leaveRandomCar().then(res => {
      //TODO AUF 
      this.update();
    }).catch((err) => { });
  }

}

