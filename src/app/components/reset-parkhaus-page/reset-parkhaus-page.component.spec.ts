import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetParkhausPageComponent } from './reset-parkhaus-page.component';

describe('ResetParkhausPageComponent', () => {
  let component: ResetParkhausPageComponent;
  let fixture: ComponentFixture<ResetParkhausPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetParkhausPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetParkhausPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
