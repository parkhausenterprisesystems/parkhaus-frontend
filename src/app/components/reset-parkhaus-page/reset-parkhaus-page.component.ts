import { Component, OnInit } from '@angular/core';
import { AbstractComponent } from '../../classes/abstract-component';
import { LoadingService } from '../../services/loading.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GeneralService } from '../../services/api/general.service';

@Component({
  selector: 'app-reset-parkhaus-page',
  templateUrl: './reset-parkhaus-page.component.html',
  styleUrls: ['./reset-parkhaus-page.component.scss']
})
export class ResetParkhausPageComponent extends AbstractComponent implements OnInit {
  form: FormGroup;
  constructor(loading: LoadingService, private fb: FormBuilder, private general: GeneralService) {
    super(loading);
    this.buildForm();
  }

  buildForm() {
    this.form = this.fb.group({
      etagen: [1, [Validators.required, Validators.min(1)]],
      parkinglots: [1, [Validators.required, Validators.min(1)]],
      disabledparking: [1, [Validators.required, Validators.min(1)]],
      womenparking: [0, [Validators.required, Validators.min(0)]],
      price: [1.00, [Validators.required, Validators.min(0.1)]]
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.form.valid && this.form.touched) {
      this.showLoading();
      this.general.resetParkhaus(this.form.value).then(res => {
        this.hideLoading();
      }).catch((err) => {
        this.hideLoading();
      });
    }
  }

}
