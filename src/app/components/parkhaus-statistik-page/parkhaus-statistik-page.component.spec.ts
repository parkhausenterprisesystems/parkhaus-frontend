import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkhausStatistikPageComponent } from './parkhaus-statistik-page.component';

describe('ParkhausStatistikPageComponent', () => {
  let component: ParkhausStatistikPageComponent;
  let fixture: ComponentFixture<ParkhausStatistikPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkhausStatistikPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkhausStatistikPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
