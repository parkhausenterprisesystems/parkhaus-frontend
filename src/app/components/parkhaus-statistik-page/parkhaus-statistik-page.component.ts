import { Component, OnInit } from '@angular/core';
import { AbstractComponent } from '../../classes/abstract-component';
import { LoadingService } from '../../services/loading.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GeneralService } from '../../services/api/general.service';
import { IfStatistikResult } from '../../data/if-statistik-result';

@Component({
  selector: 'app-parkhaus-statistik-page',
  templateUrl: './parkhaus-statistik-page.component.html',
  styleUrls: ['./parkhaus-statistik-page.component.scss']
})
export class ParkhausStatistikPageComponent extends AbstractComponent implements OnInit {
  form: FormGroup;
  stat: IfStatistikResult = null;
  maxHeightValue: number = 0;
  maxHeightKostenValue: number = 0;
  maxHeightFahrzeugeValue: number = 0;
  yheight: number = 120;
  constructor(loading: LoadingService, private fb: FormBuilder, private general: GeneralService) {
    super(loading);
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      monat: [1, [Validators.min(1), Validators.max(12)]],
      jahr: [2016, [Validators.min(2016), Validators.max(2035)]]
    });
  }

  ngOnInit() {
    let d: Date = new Date();
    this.form.setValue({ monat: d.getMonth() + 1, jahr: d.getFullYear() });
    this.onSubmit();
  }

  onSubmit() {
    if (this.form.valid) {
      this.showLoading();
      this.general.getStatistik({ monat: this.form.value.monat, jahr: this.form.value.jahr }).then((res) => {
        this.stat = res;
        this.hideLoading();
        this.maxHeightValue = this.getMaxEinnahmenValue();
        this.maxHeightFahrzeugeValue = this.getMaxFahrzeugeValue();
        this.maxHeightKostenValue = this.getMaxKostenValue();
      }).catch((err) => { this.hideLoading() });
    }
  }

  getMaxFahrzeugeValue(): number {
    let max: number = 0;
    if (this.stat != null && this.stat.jahresstatistik.length > 0) {
      this.stat.jahresstatistik.forEach(e => { if (e.anzahlfahrzeuge > max) { max = e.anzahlfahrzeuge } });
    }
    return max;
  }

  getMaxEinnahmenValue(): number {
    let max: number = 0;
    if (this.stat != null && this.stat.jahresstatistik.length > 0) {
      this.stat.jahresstatistik.forEach(e => {
        if (max < e.einnhamen) { max = e.einnhamen; }
      });
    }
    return max;
  }

  getMaxKostenValue(): number {
    let max: number = 0;
    if (this.stat != null && this.stat.jahresstatistik.length > 0) {
      this.stat.jahresstatistik.forEach(e => { if (e.ausgaben > max) { max = e.ausgaben } });
    }
    return max;
  }

  getHeightOfYear(index: number): number {
    if (this.maxHeightValue == 0) {
      return 0;
    }
    let faktor = 100 / this.maxHeightValue;
    return this.stat.jahresstatistik[index].einnhamen * faktor;
  }

  getHeightOfKostenYear(index: number): number {
    if (this.maxHeightKostenValue == 0) {
      return 0;
    }
    let faktor = 100 / this.maxHeightKostenValue;
    return this.stat.jahresstatistik[index].ausgaben * faktor;
  }


  getHeightFahrzeugeOfYear(index: number): number {
    if (this.maxHeightFahrzeugeValue == 0) {
      return 0;
    }
    let faktor = 100 / this.maxHeightFahrzeugeValue;
    return this.stat.jahresstatistik[index].anzahlfahrzeuge * faktor;
  }
}
