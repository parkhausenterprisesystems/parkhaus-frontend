import { Component, OnInit, Input, HostListener, EventEmitter, Output } from '@angular/core';
import { IfParkplatz } from '../../../data/if-parkplatz';
import { IfEtage } from '../../../data/if-etage';
import { MatDialog } from '@angular/material';
import { CarDetailsDialogComponent } from '../../../dialogs/car-details-dialog/car-details-dialog.component';
import { IfCarDetailsDialogData } from '../../../data/dialogs/if-car-details-dialog-data';

@Component({
  selector: 'app-parkplatz',
  templateUrl: './parkplatz.component.html',
  styleUrls: ['./parkplatz.component.scss']
})
export class ParkplatzComponent implements OnInit {
  @Input("value") value: IfParkplatz;
  @Input("etage") etage: IfEtage;
  @Output("update") update: EventEmitter<any> = new EventEmitter<any>();
  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  @HostListener('click', ['$event']) onClick(e) {
    this.openDetails();
  }

  isBehindert(): boolean {
    if (this.value && this.value.typ == 'BEHINDERT') {
      return true;
    }
    return false;
  }

  isFrauenParkplatz(): boolean {
    if (this.value && this.value.typ == 'FRAUEN') {
      return true;
    }
    return false;
  }


  openDetails() {
    let data: IfCarDetailsDialogData = { etageid: this.etage.id, parkplatzid: this.value.id };
    let dialogRef = this.dialog.open(CarDetailsDialogComponent, {
      data: data
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res.name == "leave") {
        this.update.emit();
      }
    });
  }
}
