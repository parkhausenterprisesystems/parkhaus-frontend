import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkplatzComponent } from './parkplatz.component';

describe('ParkplatzComponent', () => {
  let component: ParkplatzComponent;
  let fixture: ComponentFixture<ParkplatzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkplatzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkplatzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
