import { Component, OnInit, Input } from '@angular/core';
import { IfCar } from '../../../data/if-car';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {
  value: IfCar = null;
  @Input("value")
  set setValue(value: IfCar) {
    this.value = value;
  }
  constructor() { }

  ngOnInit() {
  }

  getGeschlecht(): string {
    if (this.value && this.value.geschlecht == "WEIBLICH") {
      return "Frau";
    }
    return "Mann";
  }
  getBehinderung(): string {
    if (this.value && this.value.behinderung) {
      return "Ja";
    }
    return "Nein";
  }

}
