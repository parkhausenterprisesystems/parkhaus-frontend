import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkhausOverviewComponent } from './parkhaus-overview.component';

describe('ParkhausOverviewComponent', () => {
  let component: ParkhausOverviewComponent;
  let fixture: ComponentFixture<ParkhausOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkhausOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkhausOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
