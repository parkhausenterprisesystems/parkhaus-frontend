import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IfParkhausDetails } from '../../../data/if-parkhaus-details';

@Component({
  selector: 'app-parkhaus-overview',
  templateUrl: './parkhaus-overview.component.html',
  styleUrls: ['./parkhaus-overview.component.scss']
})
export class ParkhausOverviewComponent implements OnInit {
  @Input("value") value: IfParkhausDetails;
  @Output("update") update: EventEmitter<any> = new EventEmitter<any>();
  step: number = -1;
  constructor() { }

  ngOnInit() {
  }

  setStep(id: number) {
    this.step = id;
  }

  onUpdate(event: any) {
    this.update.emit(event);
  }
}
