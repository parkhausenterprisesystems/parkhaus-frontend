import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-free-places-sign',
  templateUrl: './free-places-sign.component.html',
  styleUrls: ['./free-places-sign.component.scss']
})
export class FreePlacesSignComponent implements OnInit {
  parkingAmount: number = 0;
  @Input("value")
  set value(value: number) {
    if (value > 999) {
      this.parkingAmount = 999;
    } else {
      this.parkingAmount = value;
    }
  }
  constructor() { }

  ngOnInit() {
  }

}
