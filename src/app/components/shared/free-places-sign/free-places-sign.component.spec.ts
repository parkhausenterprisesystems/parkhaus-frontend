import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreePlacesSignComponent } from './free-places-sign.component';

describe('FreePlacesSignComponent', () => {
  let component: FreePlacesSignComponent;
  let fixture: ComponentFixture<FreePlacesSignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreePlacesSignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreePlacesSignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
