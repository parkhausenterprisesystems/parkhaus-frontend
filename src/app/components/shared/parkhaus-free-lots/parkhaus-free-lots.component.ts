import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { GeneralService } from '../../../services/api/general.service';
import { Subject } from 'rxjs/Subject';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-parkhaus-free-lots',
  templateUrl: './parkhaus-free-lots.component.html',
  styleUrls: ['./parkhaus-free-lots.component.scss']
})
export class ParkhausFreeLotsComponent implements OnInit, OnChanges {
  freeParkingLots: number = 0;
  @Input("reload") reload: Subject<any> = new Subject<any>();
  constructor(private general: GeneralService) { }

  ngOnInit() {
    this.loadData();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.reload.subscribe((res) => {
      this.loadData();
    });
  }


  loadData() {
    this.general.getParkingLotInformation().then(res => {
      this.freeParkingLots = res.free;
    }).catch(err => {

    });
  }


}
