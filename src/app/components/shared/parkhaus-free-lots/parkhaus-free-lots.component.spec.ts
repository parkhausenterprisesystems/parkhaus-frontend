import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkhausFreeLotsComponent } from './parkhaus-free-lots.component';

describe('ParkhausFreeLotsComponent', () => {
  let component: ParkhausFreeLotsComponent;
  let fixture: ComponentFixture<ParkhausFreeLotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkhausFreeLotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkhausFreeLotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
