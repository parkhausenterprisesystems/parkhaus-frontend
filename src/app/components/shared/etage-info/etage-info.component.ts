import { Component, OnInit, Input } from '@angular/core';
import { IfEtage } from '../../../data/if-etage';

@Component({
  selector: 'app-etage-info',
  templateUrl: './etage-info.component.html',
  styleUrls: ['./etage-info.component.scss']
})
export class EtageInfoComponent implements OnInit {
  @Input("frei") frei: number;
  @Input("belegt") belegt: number;
  @Input("icon") icon: string;
  constructor() { }

  ngOnInit() {
  }

}
