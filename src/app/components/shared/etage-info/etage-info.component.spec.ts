import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtageInfoComponent } from './etage-info.component';

describe('EtageInfoComponent', () => {
  let component: EtageInfoComponent;
  let fixture: ComponentFixture<EtageInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtageInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtageInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
