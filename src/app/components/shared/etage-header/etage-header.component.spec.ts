import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtageHeaderComponent } from './etage-header.component';

describe('EtageHeaderComponent', () => {
  let component: EtageHeaderComponent;
  let fixture: ComponentFixture<EtageHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtageHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtageHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
