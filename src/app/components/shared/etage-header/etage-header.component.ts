import { Component, OnInit, Input } from '@angular/core';
import { IfEtage } from '../../../data/if-etage';

@Component({
  selector: 'app-etage-header',
  templateUrl: './etage-header.component.html',
  styleUrls: ['./etage-header.component.scss']
})
export class EtageHeaderComponent implements OnInit {
  @Input("value") value: IfEtage;
  constructor() { }

  ngOnInit() {
  }

}
